var ang = 0.1; //variable ang = 0.1

function setup() {
	createCanvas(windowWidth, windowHeight);
	background(0, 0, 0);
	rectMode(CENTER)//Center the rectangle
	frameRate(60)//Framrate of 60
	noStroke()
}

function draw(){
		translate(mouseX, mouseY); //move 0,0 to center of screen
  	rotate(ang); // rotate angle degrees
		fill(random(200),random(200),random(200)); // random colors
  	rect(0,0, frameCount % 500, frameCount % 500); //draw rectangle with above affects and expand with 500% then resets
		ang = ang+0.2 //multiplies the variable with 0.2 which makes it rotate.
}
