**README miniX1**

[RUNME](https://soelvesterstaniol.gitlab.io/ap2021/miniX1/)

![](Screenshot_2021-02-07_190820.png)

What have you produced?
For our very first miniX I have produced a visual program. The product is simply rectangles that spins around leaving a trail of the previous rectangles with random colors. The only interaction you have with the program is moving the curser around, which dictates where the rectangles spawn.

The process of writing my first miniX was mainly done by trial and error. I conducted my research for my program by exploring the internet for different kinds of work I then rewrote the code I found online and added more afterwards which then resulted in my first miniX1. The experience of doing this on my own felt overwhelming at first. There is alot to comprehend and the results are often hard to achieve, but as you slowly progress closer to a complete result you feel motivated and excited. And when you get to execute the lines of code, you wrote perfectly - you feel completed. The underlying stress of not knowing what the problem with your lines of codes can be exhausting, so it is important to take breaks and reflect over what you have written.

To code is to learn a completely new language. It is important that you are dedicated to the course and practices every so often, otherwise you might forget. But by practicing and staying on track you will see how programming/learning a new language will become second nature to you.

For me coding and programming is a way to solve problems. It is an essential perk which can help you discover a new world of opportunities. And with our assigned reading I might be able to reach this next step, and hopefully gain skills that will help me in the future.

[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX1/sketch.js)
