[RUNME](https://soelvesterstaniol.gitlab.io/ap2021/miniX7/)

![](screenshotMiniX7.PNG)

Be aware, that the images might take a few seconds to load, so dont start the game immidiatly

**Describe how does your game objects work? and how are their attributes related**

I have created a small game where the goal of the game is to collect as many beers as possible. The beers are dropping from the "sky" and you collect them using the beercase which follows your mouse on the X-axis. When you collect a beer your score increases by 1, which is shown in the top left corner of the game. When you collect a beer, the speeed of the beers falling is increased, and thereby also the diffculty. Once you miss a beer you lose and the screen will show the totalt of beers caught. You can then reset the game and try again. - The game consists of two objects and serve different purposes. The first object is the "Tuborg"-beer falling and the second is the "Tuborg"-beercase. The two objects have different attributes. Like mentioned above, the attribute of the beer is falling with increasing speed, but also at random places on the top border of the windowsWidth. The beercase moves with the mouse on the X-asis, and is the main object of the game, because this is what gives the player the ablity to interact with the game. Without this, the game would be pretty boring. This is also true for the beers falling, but they serve as a smaller part of the game, and serves as a reason to keep playing(trying to beat your last score)

**Draw upon the assigned reading, what are the characteristics of object-oriented
programming and the wider implications of abstraction?**

I created my code from a P5.js sketch: [BallCatcher](https://editor.p5js.org/ehersh/sketches/Hk52gNXR7). And changed the two objects into a beer and into a beercase. I had trouble rewriting my code so my two objects(the falling beer and the case), could be called within the sketch.js as a `class`. My two object still have different attributes/porperties, which is characteristic for an object.

UPDATE 18.05.2021: I have now played around the program and changed the beers that a falling from the sky into the Class. The files name is still player.js, but it is not the beercase but the beersfalling. The reason for this change was because of the missing image when i tried to use the case of beer as the class. I changed it to the beers falling with solved this issue. 

_"Objects consist of attributes/properties and actions/behaviors, and all these hold and manage data so it can be used and operations can be performed."_ (soon & cox p.158)

The arguement of the quote now fits the beers falling, which works as the blue print for their attributes and properties.


[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX7/sketch.js)

**Refrences**

Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

[BallCatcher](https://editor.p5js.org/ehersh/sketches/Hk52gNXR7)

