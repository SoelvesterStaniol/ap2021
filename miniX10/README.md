![](flowchart_miniX10.png)

Answered with my group:

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

Creating a flowchart is a method to elaborate your program and clarify the practical and conceptual aspects of the algorithm within.

> _“Flowcharts, “Flow of the chart →  chart of the flow”, have been considered a fundamental explanatory tool since the early days of computer programming. One of their common uses is to illustrate computational operations and data processing for programming by “converting the numerical method into a series of steps.””_ (Soon & Cox 2020 p. 214)

The difficulties involved within creating a flowchart and keeping things simple at the communations level whilst maintaining the complexity is to translate everything from a coding language to the spoken language. It is almost like translating a german text in highschool for the first time. 

> _“The diagram is an “image of thought,” in which thinking does not consist of problem-solving but — on the contrary — problem-posing.”_ (Soon and Cox 2020, p. 221)  

**What are the technical challenges facing the two ideas and how are you going to address these?**

Making the flowcharts and generating different ideas for this week's MiniX, were very different from just making the program to begin with. During the making of each flowchart we were talking about how we were going to make each program, and which syntaxes we were going to use. We weren't quite sure of how to actually make each program,  but were looking around trying to figure out if the programs were even possible for us to make. We found some syntaxes, and some games that had some similarities to what we wanted, and from there we made the flowcharts knowing that the programs are possible to make, even though we are not sure yet, how to technically make them. 

**In which ways are the individual and the group flowcharts you produced useful?**

The flowcharts can be useful as a tool to communicate how you envision your idea. When making the group flowchart we also found that the flowcharts helped make clear what technical knowhow we need(ed) to create the programs we envision. While making clear what difficulties we would have technically, the flowchart also made clear how our ideas maybe weren’t as complicated as we first imagined. The flowcharts allowed us to quickly get an overview of our ideas and see if the ideas were possible for us.

My own flowchart of miniX7

![](screenshotMiniX10.png)
