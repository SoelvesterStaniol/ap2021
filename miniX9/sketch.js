let weather;
let img1, img2, img3, img4;

function preload (){
  img1 = loadImage("termometer.png");
  img2 = loadImage("cloud.png");
  img3 = loadImage("sun.png");
  img4 = loadImage("tree.png");
}

function setup() {
  createCanvas(500,600);
  loadJSON('http://api.openweathermap.org/data/2.5/weather?q=aarhus&units=metric&appid=2a8de78fe48e1635dc57c3a2e65b9b52', gotData);
}

function gotData(data) {
  //println(data);
  weather = data; 
}

function draw() {
  background(255);
  textFont("Verdana");
  text("Weather in Aarhus",20,20);
  image(img1,50,100,75,150);
  image(img4,100,150, 100, 100);
  push();
    if(weather) {
      fill(255,0,0);
      noStroke();
      rect(80, 210, 9, weather.main.temp);
      image(img3,170,80,weather.main.temp,20);
      image(img2,150, 85, weather.clouds.all, 20);
    }
    pop();
}
