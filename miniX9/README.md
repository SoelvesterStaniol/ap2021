[RunMe](https://soelvesterstaniol.gitlab.io/ap2021/miniX9/)

[RunMe but from another library](http://m.marschall.gitlab.io/ap2021/miniX9_MaltheNielsen/)
(This is linked through Mads Marcshall gitlab because of an issue that appears when uploaded from my PC)

![](screenshotMiniX9.png)

**EDIT**
Currently our program can't acces the API values/information through GitLab, only from the atom live server. 

**What is the program about? Which API have you used and why?**

The program shows the temperature in Aarhus, but it also shows if the weather is sunny or if it is cloudy. The program is taken out of inspiration from Daniel Shiffman. The API we have chosen is the one from the website Open Weather Map, and the API is called “current weather data” that gives you weather data from whatever country you require. In Daniel Shiffman's video he looked at London's weather data, but we found it very interesting how easy it is to change the data to another country, by changing the URL. We chose this API after some time trying to find different APIs on the internet. We both found it quite difficult to find a different API to use and  after time of subscribing to different websites in the hope of getting interesting APIs. At first we wanted to make a program that would count each newborn from around the world, and we even found a website where it looked like we could get an API for that, but after subscribing, nothing happened. After time looking around for other places that might have some interesting APIs, we decided to make our program about weather, which is also interesting. 

**Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data? What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?**

The process of finding data took quite a while, but afterwards it was interesting to get the program to work. Because we took inspiration from Daniel Shiffman’s video, we have also changed a few things, like the aesthetics of the program. We chose to make a thermostat, which shows the temperature in Aarhus, by using a picture of a thermostat and placing a red rectangle on top, that extends according to the temperature in the city. We also thought it could be fun to make a cloud and a sky for even more representation of the weather. 
Platform providers track movements on websites, when you allow them to by agreeing to cookies, but if you decline the cookies, there is still data that can be collected. If you agree to the cookies, you are allowed to see and do much more on a current website, than if you decline. Therefore websites have much control over the data you create while looking around on the current website. The data collected is most of the time used for the owners of the site, to know what users are mostly interested in, and what they are not interested in. This can help businesses to make even better sites, and sell more. On the other hand, the data collected can be used and stored to create personalised recommendations and advertisements, for each individual user. Every click collects data, and every piece of data collected about you, can be used to categorize who you are, and where you belong. Because of this categorization, the recommendations you get are most likely different from what your friends and family are recommended. Therefore it is also easy for companies to manipulate users to buy or see something because they are targeted. That is also why when you have been on a website, you are very much likely to see advertisements from that site, next time you open your Facebook account. 

> _“To que(e)ry data in this way throws into further question how data is collected, stored, analysed, recommended, ranked, selected, and curated in order to understand the broader social and political implications, not least how categorizations such as gender and race are normalized and hegemonized. To query the power structures of materials from a feminist standpoint is to understand “the mechanisms that shape reality” and how they might be reprogrammed” (Cox & Soon, p 207)_

**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**

The Weather Map says: “Access current weather data for any location on Earth including over 200,000 cities! We collect and process weather data from different sources such as global and local weather models, satellites, radars and vast networks of weather stations” But we could not find any specific references to where this data comes from, which is interesting and something we would like to have looked more into. This is generally an interesting topic in concern of other API’s as well. Where does the data come from? Who and what makes the data and decides what data is most accurately and what is not? That is why we find it most important to stay critical to the data in the API.



[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX9/sketch.js)

**References:**

Soon Winnie & Cox, Geoff, "Que(e)ry Data", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186	

Open Weather Map:
https://openweathermap.org/api 

https://p5js.org/ 

Daniel Shiffmann 10.5: Working with APIs in Javascript - p5.js Tutorial: https://www.youtube.com/watch?v=ecT42O6I_WI&t=739s 
