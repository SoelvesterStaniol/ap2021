let angle = 0;

function setup(){
  createCanvas(windowWidth,windowHeight)
}

function draw(){
  background(51,10)
  first();
  second();
}

function first() {
  push();
  translate(width/2, height/2);
  rotate(angle);
  strokeWeight(5)
  stroke(255);
  line(0,50,100,0);
  angle += 0.1;
  pop();
}

function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
}
