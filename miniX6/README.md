[RUNME](https://soelvesterstaniol.gitlab.io/ap2021/miniX6/)

![](screenshot_miniX6.png)

**Which MiniX do you plan to rework?**
For this miniX our objective is to rework one of our earlier projects so we can catch up and explore further with the previous learnt functions and syntaxes and also revisit the past, design iteratively based on the feedback and reflection on the subject matter and to further reflect upon the methodological approach of "Aesthetic Programming". For my miniX6 i have chosen to rework my miniX3. I thought it was clumsy and not well made. So i have chosen to rework it from scratch and create a new throbber based on the same function and syntaxes we practiced then. The reason for the complete makeover is also because i like to keep my work minimalistic, which the earlier definetly wasn't. I have now created a very simple throbber that is smoother and suits a program better. I wanted to create something with lowcieling consindering how much knowledge i have gained from the miniX3. Creating something as simple as this throbber, was alot easier this time because i have a better understanding of how the program works. This way i ackownledge how much i have learned, and looking back how much i have developed. 

Aesthetic programming tries to look at programming not just in the sense of code and language, but also with thoughts around society, culture, behavior and more. In this program the asthetics is primarily visualised through color and simplicity. Through the years of the 70's, 80's, 90's and 20's and earlier, design has been evolving at a very fast paced. Modern music, architecture and more are all inspired by the minimalistic culture. When visiting a homepage the same inpiration applies. We as consumers dont want unnesseseary content on the pages we visit. The website has to be smooth, quick and nice to look at. Too much information on a websites becomes confusing for the consumer. A more pleasing website and easy shortcuts create a better experience for the user.

[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX2/sketch.js)

**Refrences**
Soon Winnie & Cox, Geoff, "Infinite loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 77
