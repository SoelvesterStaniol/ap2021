[RUNME](https://soelvesterstaniol.gitlab.io/ap2021/miniX3/)

![](Assets/Screenshotminix3.png)

**What do you want to explore and/or express?**

For this miniX assignment i have been exploring how to create a throbber. For creating my throbber i used the throbber template from Soon Cox Aesthetic Programming. With the code i have been changing the different syntaxes and creating my own code to create the final product. This program consist of a "scope"-throbber and a person within sight of the scope. At first i wanted to draw two different pictures of a running man, and shifting between these two would make him run in the same place. But because of a busy week and weekend i only aimed to create a simple man with a throbber. And while creating it, the throbber started to look like a scope and i moved the man and throbber to the center and thats what created this throbber. I am not aiming to express anything particular in this throbber.

Considering the code for this throbber, the time that is related to the throbber is infinite. The screen will always keep loading, but if it was going to be used for a 'real' throbber inside a game, i would like the screen to turn red behind the man as it progresses closer to fully load the program.
Thinking about my throbber, and how it could be used in digital culture, i woul like to think, that my throbber could be used in a game. 

[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX3/sketch.js)
