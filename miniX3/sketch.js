//throbber
function setup() {
 //create a drawing canvas
 createCanvas(750, 600);
 frameRate (10);
}

function draw() {
  background(0);
  drawElements(); //calling a new function
  }

function drawElements() {
  let num = 9;
  push();
  noFill();
  stroke(255,255,255);
   strokeWeight(3);
    bezier(503,256,452,324,455,291,374,277); //arm (right)
    stroke(255,255,255);
    strokeWeight(3);
    bezier(374,303,374,245,205,254,301,340); //arm
    stroke(255,255,255);
    strokeWeight(3);
    bezier(325,436,500,503,417,382,363,381);  //leg
    stroke(255,255,255);
    strokeWeight(3);
    bezier(405,508,436,377,376,382,360,381); //leg
     noStroke();
      fill(255,255,255);
       rect(341,258,69,130,200);
        ellipse(375,221,80,80);
  //move things to the center
  translate(width/2, height/2);
  /* 360/num >> degree of each ellipse's movement;
  frameCount%num >> get the remainder that to know which one
  among 8 possible positions.*/
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255, 20, 0);
  //the x parameter is the ellipse's distance from the center
  ellipse(0,0,5,5)
  ellipse(20, 0, 12, 12);
  pop();
  stroke(255, 255, 255,18);
  strokeWeight(50)
  //static lines
  line(60, 0, 60, height);
  line(width-60, 0, width-60, height);
  line(0, 60, width, 60);
  line(0, height-60, width, height-60);

  text("Loading...",150,150);
  textSize(20)

}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
