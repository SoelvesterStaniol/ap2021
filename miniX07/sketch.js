let screen = 0;
let y=-20;
let x=200;
let speed = 2;
let score= 0;
let img;
let beerCase;
let myPlayer;

function preload () {
  img = loadImage("ceresBeer.png");
  beerCase = loadImage("beerCase.jpg")
}

function setup() {
  createCanvas(600, 400);

  beers = new CeresBeer();
}

function draw() {
beersMove();
switchScreen();
}

function startScreen(){
		background(96, 157, 255)
		fill(255)
		textAlign(CENTER);
		text('WELCOME TO CATCH THE BEERS', width / 2, height / 2)
		text('click to start', width / 2, height / 2 + 20);
		reset();
}

function gameOn(){
  image(beerCase,mouseX,height-40,70,50)
}

function pickRandom(){
	x= random(20,width-20)
}

function endScreen(){
		background(150)
		textAlign(CENTER);
		text('GAME OVER', width / 2, height / 2)
  	text("SCORE = " + score, width / 2, height / 2 + 20)
		text('Reload the page to play again', width / 2, height / 2 + 40);
}

function mousePressed(){
	if(screen==0){
  	screen=1
  }else if(screen==2){
  	screen=0 //Does not work because of beersMove() and switchScreen() are in the draw function. tried to move to the setup, but it stops the game prematurely
  }
}

function beersMove(){
  beers.move();
}

function switchScreen(){
	if(screen == 0){
    startScreen()
  }else if(screen == 1){
  	gameOn();
  }else if(screen==2){
  	endScreen()
  }
}

function reset(){
	  score=0;
  	speed=2;
  	y=-20;
}
