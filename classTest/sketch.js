let x = 0;
let y = 0;
let spacing = 100;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  frameRate(120)
}

function draw() {
  fill(random(255))
  rect(x+10,y-10,30,30)
  fill(random(255)  )
  rect(x,y,30,30);
    triangle(x, y, x+30, y, x+15, y-50);
  if (random(2) < 0.5) {
    line(x, y, x+spacing, y);
  } else {
    line(x, y+spacing, x+spacing, y);
  }
  x+=50;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
