let vert = 0
function setup() {
	createCanvas(500, 500);
}

function draw(){
	background(0, 20, 100);

stroke(50);
strokeWeight(2)
fill(255,255,255);
arc(300,360,120,120,4,PI); //semi circel
fill(0,20,100);
arc(300,360,100,80,4,PI);

noStroke()
	fill(255,255,255); //skummet på øllen
	ellipse(250,250,20,20);
	fill(255,255,255);
	ellipse(240,250,20,20);
	fill(255,255,255);
	ellipse(220,250,20,20);
	fill(255,255,255);
	ellipse(230,250,20,20);
	fill(255,255,255);
	ellipse(210,250,20,20);
	fill(255,255,255);
	ellipse(200,250,20,20);
	fill(255,255,255);
	ellipse(195,250,20,20);
	fill(255,255,255);
	ellipse(195,260,20,20);
	fill(255,255,255);
	ellipse(230,245,20,20);
	fill(255,255,255);
	ellipse(220,245,20,20);
	fill(255,255,255);

strokeWeight(2)
stroke(50);
	fill(255,204,0);
	rect(190,250,120,200); //ølglasset
noStroke()
	fill(255,255,255)
	rect(190,250,120,20) //ølskummet i glasset

noStroke()
	ellipse(218,250,20,20); //skummet uden på øllen
	fill(255,255,255)
	ellipse(195,255,20,20);
	fill(255,255,255)
	ellipse(230,255,20,20);
	fill(255,255,255)
	ellipse(220,270,20,20);
	fill(255,255,255)
	ellipse(225,270,20,20);
	fill(255,255,255)
	ellipse(220,280,20,20);
	fill(255,255,255)
	ellipse(195,270,20,20);


	if(vert<=200) {vert=350}		//boblerne som stiger i øllen
	noStroke()
	fill(255,255,255);
	ellipse(270,vert+95,10,10)
	ellipse(215,vert+90,8,8)
	ellipse(250,vert+70,10,10)
	ellipse(210,vert+60,10,10)
	ellipse(200,vert+95,5,5)
	ellipse(290,vert+60,5,5)

  vert=vert-2.5

fill(255,205,0)  //Smiley med mundbind
ellipse(125,125,175,175)
fill(255,255,255)
ellipse(100,100,40,40)
fill(255,255,255)
ellipse(150,100,40,40)
fill(0)
ellipse(100,100,20,20)
fill(0)
ellipse(150,100,20,20)

stroke(255)
strokeWeight(10)
line(40, 100, 80, 150);
line(40, 150, 75, 170);
line(205, 95, 150, 170);
line(210, 140, 170, 170);
noStroke()
fill(0,196,255)
rect(75, 140, 100, 50, 20);


}
