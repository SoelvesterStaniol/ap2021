[RUNME](https://soelvesterstaniol.gitlab.io/ap2021/miniX2/)

![](Assets/screenshotMiniX2.PNG)

**Describe your program and what you have used and learnt.**

My program consists of 2 emojis. The first emoji is a smiley with a mouthpiece and the other is a beer with rising bobles. The most important skill i can take with me from this excersise is the experience using different shapes in 2D. Other than that i also polished my skills in generel, using variables and other syntax's. To complete this assignment, it reguired a lot of thought about the geometric values and spacial relations. In addition the visual composistion also played a big role. Deciding where you place the different shapes compared to eachother and the background will play a role for how the emojis are viewed in hole. Nothing is coded by accident. 

**How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it).**

The context of the 2 emojis viwed alone is very different from eachother. The mouthpiece emoji can easily represent the global pandamic we're facing right now, but before the pandamic the mouthpiece would probably represent doctors, dentist etc. Because the world has changed a lot, the view of the emoji has changed aswell. It wasn't custom to wear a mouthpiece in Europe by poeple in social settings before, but now it would be weird to see a person without inside a store. Compared to some asian countries, where wearing a mouthpiece has been a generel custom for many years. The beer emoji alone, often symbolizes celebration, friendship etc. We drink beer when we go out, get off work or are just hanging out with friends. This is common all over the world, tho the beer may vary in different kinds of liquor. Side by side with the mouthpiece emoji, the 2 emojis might represent some of the cultural norms that we are sacrafising to reach a normal life again. This is one way why visual composition is important and how it can change your perspective to the program.

[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX2/sketch.js)
