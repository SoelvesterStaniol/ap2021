//variables used throughout the code
let buttonYes;
let buttonNo;
let img;
let reject = ["Are you sure?", "Dont you want to see the page?", "The content will blow your mind!", "Allow Cookies to view this site", "You wont regret it!", "All your friends will be talking about it", "It's only cookies", "Mind blowing content right here", "Just a single click and you can read this article!", "I think you missed", "Try again!", "Almost there!", "Just a little to the left"]
let allow = ["(Thank you for allowing cookies)"]
let error = ["404", "Not Found", "The resources requested could not be found on this server!"]

//loading image
function preload() {
  img = loadImage('background.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  image(img, -125, 0); //puts the images in a whised position

//buttonYes = allowing the cookies
  push();
  buttonYes = createButton('Allow Cookies');
  buttonYes.position(windowWidth/2-240, windowHeight/2);
  buttonYes.mousePressed(change1) //calling function -> what happens when the button is pushed
  pop();

//buttonNo = rejecting the cookies.
  push();
  buttonNo = createButton('Reject Cookies');
  buttonNo.position(windowWidth/2+75, windowHeight/2);
  buttonNo.mousePressed(change2); //calling function -> what happens when the button is pushed
  pop();

//style for buttonYes
  buttonYes.style("background", "#4CAF50");
  buttonYes.style("border", "none")
  buttonYes.style("color", "white")
  buttonYes.style("text-align", "center")
  buttonYes.style("text-decoration", "none")
  buttonYes.style("display", "inline-block")
  buttonYes.style("font-size", "16px")
  buttonYes.size(150,50)

//style for buttonNo
  buttonNo.style("background", "#f44336");
  buttonNo.style("border", "none")
  buttonNo.style("color","white")
  buttonNo.style("text-align","center")
  buttonNo.style("text-decoration","none")
  buttonNo.style("display","inline-block")
  buttonNo.style("font-size","16px")
  buttonNo.size(150,50)
}

//changes when you push buttonYes
function change1() {
  background(255); //change background to white
  textSize(8); //individual textSize
  buttonYes = text(allow, windowWidth/2-60, windowHeight/2+150); //call array "thanks for..."
  textSize(150);
  buttonYes = text(error[0], windowWidth/2-140, windowHeight/2-75); //call array "404"
  textSize(25)
  buttonYes = text(error[1], windowWidth/2-65, windowHeight/2+100); //call array "Not Found"
  textSize(15)
  buttonYes = text(error[2], windowWidth/2-200, windowHeight/2+135); //call array "the resources..."
  fill(0); //color for text
}

//changes when you push buttonNo
function change2() {
  background(img); //change background to img (So the text doenst stack)
  //rectangle behind text
  push();
  fill("grey")
  noStroke()
  rect(330,240,850,50,5)
  pop();
  //text in the rectangle
  textSize(32);
  textStyle(BOLD)
  buttonNo = text(random(reject), windowWidth/2-340, windowHeight/2-100); //choses random from reject array
  fill(0); //color for text
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
