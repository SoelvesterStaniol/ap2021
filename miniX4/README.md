[RUNME](https://soelvesterstaniol.gitlab.io/ap2021/miniX4/)

![](assets/ScreenshotMiniX4.png)
 
**Provide a title for and a short description of your work (1000 characters or less)
as if you were going to submit it to the festival.**

_**"Not sharing is caring"**_ This work is meant to illustrate how most websites ask for permission to save cookies on your compute for you to access their content. Digital cookies are small text files that are stored on your computer when you visit a website. the websites then use the cookies so they can recognize computers and remember your preferences, settings and account information to make the user experience better. For session mangement, the websites can recognize its users and recall their login information and preferences. Cookies are also used to personalize your experience on the website. It will customize your advetisements and you will be targeted by products that you might enjoy. This way shopping sites also track the items you view on their site so it can suggest you similar goods. Cookies are in itself not "harmful", but this a way for companies to keep track of people and aim their products directly at individuals. Companies close off their content if users doenst allow the website to plant cookies in their browser. Some websites even use thrid party advetisements, these will also generete cookies for companies you maybe wouldnt normally allow to plant cookies. 

**Describe your program and what you have used and learnt.**

My program consists of two buttons and a blurred background. The program wants the user to allow the program to plant cookies on their browser, so the user can read the article. When "Allow Cookies" is pressed, the page doenst load and an 404 error is presented. Beneath the text, the websites still thanks the user for allowing cookies, even tho page doesn't load. When you reject the cookies the page doesn't load and it ask's you if it was on purpose you clicked reject and that you will regret not reading this article. I diceded to make the allow button green and the reject button red, so that you user is more likely to click the allow cookies button.

**Articulate how your program and thinking address the theme of “capture all.”**

In relations to "Capture All", this program is meant to empathize how companies aquire data from their users and use this data to "personalize" their experience using the web. When you accept cookies you give the website permission to capture your data. They will capture your behaviour on the web and make sure that the ads that target you are catagorized from the data captured. The program provides a clear example how websites are using headlines, blurred backgrounds and other elements to capture your interest. 


**What are the cultural implications of data capture?**

Data capturing online can be harmful on different parametres. The data captured from online users are used to affect the users indirectly.
_"For example, it is easy to track the cursor’s position and compute the duration of its stay in different areas of a web page, providing an indication as to which content is “hotter” than the rest. This is useful for marketing purposes, not least to understand which content is more or less attractive to users, and for companies or political parties to analyze where to best place their ads and other propaganda"_ (Soon & Cox p. 111-112)
This was proven when the Facebook-Cambridge Analytica scandal happend. It was revealed in 2018 that the personal data of milions of Facebook users had been harvested without consent and used for political advertisement. This resulted in big lawsuits against Facebook.
"Shoshana Zuboff on Surveillance Capitalism" covers different angels of this and other cases in her documentary, where she comments on how this leak of personal data is harmful in many ways.


[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX4/sketch.js)


