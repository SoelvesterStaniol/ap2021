let x = 0;
let y = 0;
let spacing = 15; //the variable that sets the size and spacing for each "letter"/bezier

function setup() {
  createCanvas(600, 600);
  background(0);
}

function draw() {
  noFill();
  stroke(255);

//creates the different "letters"/bezier otherwise create a blank space
  if (random(1) < 0.8) {
    bezier(x+random(spacing), y+random(spacing), x+random(spacing), y+random(spacing), x+random(spacing), y+random(spacing), x + random(spacing), y + random(spacing));
  }
//resets when x hits the edge
  x += random(spacing);
  if (x > width) {
    x = 0;
    y += spacing;
//resets when y hits the bottom (this creates a new "Chapter")
  if (y > height) {
    y = 0;
    x += spacing;
    background(0) //clears the background
  }

  }
//creates the pink flowers in the background(Skaura-season)
  for(var i=0;i<1;i++){
    fill(249,137,234,15);
    noStroke();
    ellipse(random(600),random(600),random(50));
}
}
