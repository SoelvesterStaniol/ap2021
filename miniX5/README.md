[RUNME](https://soelvesterstaniol.gitlab.io/ap2021/miniX5/)

![](assets/screenshotMiniX5.png)

**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior? And what role do rules and processes have in your work?**

For this miniX we were instructed to create our miniX based on rules that should be implemented in the program. These rules should be thought of before starting to code. This was a tough task, but after a lot of consideration and struggeling i created two rules based of the 10PRINT slasforward and backward example from class05 which is also shown in Daniel Schiffmans youtube challenge(Link in refrence). My rules are following almost the same princeples, "Throw a dice and print a backslash half the time"
"Print a forward slash the other half of the time". My rules tho simliar follows these princeple:

    1. Pick a number between 0 and 0.8 and print one random bezier
    2. Otherwise print nothing

The rules are defining how my generative program performs over time. This is also the core of 10PRINT and how it may differ from other generative programs _"The core difference with 10PRINT is the way in which it generates complex,emergent behavior using only a simple set of rules."_(Soon & Cox p. 126). The idea of the program is that the background peforms as a chalkboard/black piece of paper, and that the "bezier" is different letters/words. When performing the code i noticed that the letters had an asian caracteristica. That led me to create the pink-ish background which consists of almost see through pink circles. I chose pink because we are close to the Sakura-season in Japan. 

The role of the rules in my program is the reason for its unique look and 10PRINT structure. While the code is running, it’s creating this sheet of paper covered in made-up words. I believe that the blank space created rarely from rule number 2 is very important for the imagined writing. This create the sentece-like structure. When the page then run out of space the program starts over creating a new chapter.

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care viarules)?**

Even tho i would describe my letters as complete random, it is argued that the `random()` function isn't truerandom. 

_"RND is, like most computational sources of randomness, a pseudorandom number generator. While there may be no apparent pattern between any two   numbers, each number is generated based on the previous one using a deterministic process."_ (Montfort p.144) 

They argue in the text that when the first number is the same, the entire sequence will always be the same. Eventho pseudorandomness may sound inconvinent, the aurthors also cast light upon how it is generelly acceptable and in many situations desirable. That Engineers use pseudorandomness to test the program, and that every run on the program needs these variables to have the same rules, which otherwise would create problems when the test is repeated. - My program has helped me understand how the `random()` is so effective. When running the code over and over it is extremely difficult, if not impossible, to find two exactly the same `bezier()`. This illustrates how the level of control for the generative program lies with both the computer and the artist/programmer.

[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX5/sketch.js)

**Refrences**

Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA: MIT Press, 2012, pp. 119-146.

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

[Daniel Shiffman 10PRINT](https://www.youtube.com/watch?v=bEyTZ5ZZxZs&ab_channel=TheCodingTrain)

[bezier](https://p5js.org/reference/#/p5/bezier)
