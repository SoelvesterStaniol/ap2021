[RUNME](https://soelvesterstaniol.gitlab.io/ap2021/miniX8/)

![](screenshotMiniX8.png)

**Provide a title of your work and a short description with and how the program works(1,000 characters or less).**

**THE TRUTH**

Our work consists of a background showing a covid-19 vaccine. This background is slowly being covered up by anti-vaccination stanpoints. This anti-vax agenda takes over the screen and slowly eats away at the science that is the covid-19 vaccine. We made this project to showcase how we sometimes feel that logic and reason gets muddled by more and more shitty opinions from people who refuse to accept expert opinions. We se these shitty opinions from everyday people to top politicians, something we find truly idiotic and extremely irratating.
	
Our program works by using a syntax that is new to us: loadJSON. This syntax allows us to call data from a .json file into our .js file. We then use the date from the .json file to make it visible text on a canvas. Using JSON and the loadSound syntax is new to us, but the rest of the syntax used is syntax we have tried using before.

**Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language)**

Cox and McLean conclude that although programming languages are clearly not spoken, they express similar qualities to speech, which also extends our understanding of speech. Furthermore they elaborate their argument:

_“Computer code has both a legible state and an executable state, namely both readable and writable states at level of language itself.” (Cox & McLean p.37)_

And stresses that this is the point to consider code for its speaklike qualities. These exact arguments are why we consider our own program to be Vocable coding. We are creating a political debate/thoughts of critique on how some people are blind to facts and ignore science through our coding. We are using comments that has already been spoken by other people through the hashtag #Antivax on twitter and on other forums. 

To further explain Vocable code, Soon and Cox investigate how vocable code is more than just code, it is often the voicing of one's opinions. This can be both personal or political:

_“Vocable Code has a direct relation to bodily practices, the act of voicing something, and how the voice resonates with political practices.” (Soon & Cox, p. 182)_

This voicing of opinions and politics is something we have added into our program with the voice over of Donald Trump being firm in calling the covid-19 virus the “Chinese Virus”. (Trump calling covid-19 "Chinese Virus". ). The clip is an interview/press conference where Trump is questioned by his use of the word. We wanted to share the sound from this clip, because it is exactly this misinformation from powerful people that can damage the importance of research and facts. People are guided to think that the coronavirus is utter bullshit. 

[Source of code](https://gitlab.com/SoelvesterStaniol/ap2021/-/blob/master/miniX8/sketch.js)

**References:**

Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186

Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.

Trumps calling covid-19 “Chinese Virus”
https://www.youtube.com/watch?v=dl78PQGJpiI

Twitter antivax hashtag
https://twitter.com/hashtag/antivax?src=hashtag_click

Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.

Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r
