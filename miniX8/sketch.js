let data;
let img;
let sound;

function preload (){
    data = loadJSON("text.json");
    img = loadImage("covid.jpg");
    sound = loadSound("covidTrump.mp3");
}

function setup (){
  createCanvas(1200,700);
  background(img);
  console.log(data);
  frameRate(1)
  sound.play();
}

function draw (){
    for (let i = 0; i < 10; i++){
      textSize(random(50),25);
      fill(0);
      text(data[i].statement, random(windowWidth), random(windowHeight));
    }
}

function mousePressed(){
  clear();
  background(img);
}
